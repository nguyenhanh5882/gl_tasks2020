set(SRC_FILES
		${PROJECT_SOURCE_DIR}/common/Application.cpp
		${PROJECT_SOURCE_DIR}/common/DebugOutput.cpp
		${PROJECT_SOURCE_DIR}/common/Camera.cpp
		${PROJECT_SOURCE_DIR}/common/Mesh.cpp
		${PROJECT_SOURCE_DIR}/common/ShaderProgram.cpp
		${PROJECT_SOURCE_DIR}/common/Texture.cpp
)

set(HEADER_FILES
		${PROJECT_SOURCE_DIR}/common/Application.hpp
		${PROJECT_SOURCE_DIR}/common/DebugOutput.h
		${PROJECT_SOURCE_DIR}/common/Camera.hpp
		${PROJECT_SOURCE_DIR}/common/LightInfo.hpp
		${PROJECT_SOURCE_DIR}/common/Mesh.hpp
		${PROJECT_SOURCE_DIR}/common/ShaderProgram.hpp
		${PROJECT_SOURCE_DIR}/common/Texture.hpp
)

MAKE_SAMPLE(gl4_Texturing)
MAKE_SAMPLE(gl4_Magnification)
MAKE_SAMPLE(gl4_Minification)
MAKE_SAMPLE(gl4_CubeMap)

COPY_RESOURCE(gl4shaders)

add_dependencies(gl4_Texturing gl4shaders)
add_dependencies(gl4_Magnification gl4shaders)
add_dependencies(gl4_Minification gl4shaders)

