#pragma once

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <GL/glew.h>

#include <string>
#include <map>
#include <memory>
#include <vector>
#include "Common.h"
#include <iostream>

/**
Абстракция буфера с данными в видеопамяти
*/
class DataBuffer
{
public:
    /**
    Создает буфер с данными
    \param target тип буфера (GL_ARRAY_BUFFER, GL_TEXTURE_BUFFER и другие)
    */
    DataBuffer(GLenum target = GL_ARRAY_BUFFER):
        _target(target)
    {
	    if (USE_DSA) {
		    glCreateBuffers(1, &_vbo);
	    }
	    else {
		    glGenBuffers(1, &_vbo);
	    }
    }

    ~DataBuffer()
    {
        glDeleteBuffers(1, &_vbo);
    }

    /**
    Копирует данные из оперативной памяти в видеопамять, выделяя память под данные при необходимости
    \param size размер данных в байтах
    \param data указатель на начало массива данных в оперативной памяти
    */
    void setData(GLsizeiptr size, const GLvoid* data)
    {
        glBindBuffer(_target, _vbo);
        glBufferData(_target, size, data, GL_STATIC_DRAW);
        glBindBuffer(_target, 0);
    }

    void initStorage(GLsizeiptr size, const GLvoid* data, GLbitfield flags) {
        assert(USE_DSA);
        glNamedBufferStorage(_vbo, size, data, flags);
    }

    void bind() const
    {
        glBindBuffer(_target, _vbo);
    }

    void unbind() const
    {
        glBindBuffer(_target, 0);
    }

    /**
    Возвращает идентификатор шейдера
    */
    GLuint id() const { return _vbo; }

protected:
    DataBuffer(const DataBuffer&) = delete;
    void operator=(const DataBuffer&) = delete;

    ///Идентификатор буфера
    GLuint _vbo;

    ///Тип буфера
    GLenum _target;
};

typedef std::shared_ptr<DataBuffer> DataBufferPtr;

/**
Абстракция полигональной модели
Инкапсулирует:
- управляющий объект VertexArrayObject
- буферы с вершинными атрибутами VertexBufferObject (один буфер может содержать данные для нескольких атрибутов)
- тип рисуемых примитивов
- количество вершин в модели
- матрицу модели (local to world)
*/
class Mesh
{
public:
    Mesh() :
        _primitiveType(GL_TRIANGLES),
        _vertexCount(0)
    {
        glGenVertexArrays(1, &_vao);
    }

    ~Mesh()
    {
        glDeleteVertexArrays(1, &_vao);
    }
    
    /**
    Устанавливает параметры вершинного атрибута полигональной модели
    \param index номер атрибута (0, 1, 2, ...)
    \param size количество компонентов в атрибуте (1, 2, 3 или 4)
    \param type тип данных компонентов атрибута (GL_BYTE, GL_UNSIGNED_BYTE, GL_SHORT, GL_UNSIGNED_SHORT, GL_INT, GL_UNSIGNED_INT, GL_HALF_FLOAT, GL_FLOAT, GL_DOUBLE, GL_FIXED и другие)
    \param normalized должны ли данные быть нормализованы на интервал 0..1 (GL_TRUE) или использоваться как есть (GL_FALSE)
    \param stride расстояние в байтах между атрибутами двух последовательных вершин. Если 0, то атрибуты расположены в буфере плотно (без промежутков)
    \param offset сдвиг в байтах от начала буфера
    \param buffer буфер с данными, где хранятся значения атрибута
    */
    void setAttribute(GLuint index, GLint size, GLenum type, GLboolean normalized, GLsizei stride, GLuint offset, const DataBufferPtr& buffer)
    {
        _buffers[index] = buffer; //чтобы буфер не был удален раньше модели

        glBindVertexArray(_vao);

        buffer->bind();
        glEnableVertexAttribArray(index);
        glVertexAttribPointer(index, size, type, normalized, stride, reinterpret_cast<void*>(offset));
        buffer->unbind();

        glBindVertexArray(0);
    }

    /**
     * Устанавливает параметры вершинного атрибута полигональной модели.
     * Версия для signed/unsigned byte/short/int типов.
     */
    void setAttributeI(GLuint index, GLint size, GLenum type, GLsizei stride, GLuint offset, const DataBufferPtr& buffer)
    {
        _buffers[index] = buffer; //чтобы буфер не был удален раньше модели

        glBindVertexArray(_vao);

        buffer->bind();
        glEnableVertexAttribArray(index);
        glVertexAttribIPointer(index, size, type, stride, reinterpret_cast<void*>(offset));
        buffer->unbind();

        glBindVertexArray(0);
    }

    /**
    Устанавливает разделитель для вершинного атрибута (разбирается на 10м семинаре, используется при инстансинге)
    \param index номер атрибута (0, 1, 2, ...)
    \param divisor если 0, то одно значение атрибута соответствует одной вершине, если >0, то одно значение атрибута соответствует всем вершинам для divisor инстансов модели
    */
    void setAttributeDivisor(GLuint index, GLuint divisor)
    {
        glBindVertexArray(_vao);

        glVertexAttribDivisor(index, divisor);

        glBindVertexArray(0);
    }

    /**
    Устанавливает тип примитива (GL_POINTS, GL_LINES, GL_TRIANGLES и другие)
    */
    void setPrimitiveType(GLuint type) { _primitiveType = type; }

    /**
    Устанавливает количество вершин, которые должны быть отрендерены
    */
    void setVertexCount(GLuint count) { _vertexCount = count; }

    void setIndices(GLuint indicesCount, const DataBufferPtr &indexBuffer) {
        _hasIndices = true;
        _indicesCount = indicesCount;
        _indexBuffer = indexBuffer;

        glBindVertexArray(_vao);
        indexBuffer->bind();
        glBindVertexArray(0);
    }

    /**
    Матрица модели (преобразует локальные координаты в мировые)
    */
    glm::mat4 modelMatrix() const { return _modelMatrix; }

    /**
    Устанавливает матрицу модели
    */
    void setModelMatrix(const glm::mat4& m) { _modelMatrix = m; }

    GLuint getTrianglesCount() const {
        if (_hasIndices)
            return _indicesCount / 3;
        else
            return _vertexCount / 3;
    }

    /**
    Рисует модель
    */
    void draw() const
    {
        glBindVertexArray(_vao);
        if (_hasIndices) {
            glDrawElements(_primitiveType, _indicesCount, GL_UNSIGNED_INT, nullptr);
        }
        else {
            glDrawArrays(_primitiveType, 0, _vertexCount);
        }
    }

    void drawArrays(GLint first, GLsizei count) {
        assert(!_hasIndices);
        glBindVertexArray(_vao);
        glDrawArrays(_primitiveType, first, count);
    }

    /**
     * Запускает multiDraw по одним и тем же смещениям.
     * Ненатурально, используется для демонстрации.
     * Случай с несколькими мэшами в одном буфере потребует задания смещений.
     */
    void multiDraw(GLsizei drawCount) {
        glBindVertexArray(_vao);
        if (_hasIndices) {
            std::vector<GLsizei> counts(drawCount, _indicesCount);
            std::vector<const GLvoid*> offsets(drawCount, 0);
            glMultiDrawElements(_primitiveType, counts.data(), GL_UNSIGNED_INT, offsets.data(), drawCount);
        }
        else {
            std::vector<GLint> offsets(drawCount, 0);
            std::vector<GLsizei> counts(drawCount, _vertexCount);
            glMultiDrawArrays(_primitiveType, offsets.data(), counts.data(), drawCount);
        }
    }

	void multiDrawArrays(GLsizei drawCount, const std::vector<GLint> &offsets, const std::vector<GLsizei> &counts) {
		assert(!_hasIndices);
		glMultiDrawArrays(_primitiveType, offsets.data(), counts.data(), drawCount);
	}

    /**
    Рисует модель instanceCount раз (разбирается на 10м семинаре)
    */
    void drawInstanced(unsigned int instanceCount) const
    {
        glBindVertexArray(_vao);
        if (_hasIndices) {
            glDrawElementsInstanced(_primitiveType, _indicesCount, GL_UNSIGNED_INT, nullptr, instanceCount);
        }
        else {
            glDrawArraysInstanced(_primitiveType, 0, _vertexCount, instanceCount);
        }
    }

	GLsizei getVertexCount() const { return _vertexCount; }

    GLuint getVAO() const { return _vao; }

protected:
    Mesh(const Mesh&) = delete;
    void operator=(const Mesh&) = delete;

    ///Идентификатор Vertex Array Object
    GLuint _vao;

    ///Буферы с данными - храним здесь, чтобы они не были удалены раньше модели
    std::map<GLuint, DataBufferPtr> _buffers;

    DataBufferPtr _indexBuffer;

    ///Тип геометрического примитива
    GLuint _primitiveType;

    ///Количество вершин в модели
    GLuint _vertexCount;

    bool _hasIndices = false;

    GLuint _indicesCount = 0;

    ///Матрица модели (local to world)
    glm::mat4 _modelMatrix;
};

typedef std::shared_ptr<Mesh> MeshPtr;

//=========== Функции для создания тестовых мешей

/**
Создает модель сферы
*/
MeshPtr makeSphere(float radius, unsigned int N = 100);

/**
Создает модель куба размером 2 * size
*/
MeshPtr makeCube(float size);

/**
Создает прямоугольник (2 треугольника), заполняющий экран (координаты в Clip Space)
*/
MeshPtr makeScreenAlignedQuad();

/**
Создает плоскость земли размером от -size до +size по осям XY
Генерирует текстурные координаты, так чтобы на плоскости размещалось 2 * numTiles по каждой оси
*/
MeshPtr makeGroundPlane(float size, float numTiles);

class aiMesh;
MeshPtr loadFromAIMesh(const aiMesh &sourceMesh);

/**
Загружает меш из внешнего файла с помощью библиотеки Assimp
*/
MeshPtr loadFromFile(const std::string& filename, int meshIndex = 0);

MeshPtr makeCircle(float radius, unsigned int sectorCount = 3600);

// create cylinder
MeshPtr makeCylinder(float radius, float height, unsigned int sectorCount = 3600);

class Tree
{
public:
    Tree() {}
    Tree(std::string str, float radius, double angle, unsigned int depth, float height, float num) :
        str(str), radius(radius), angle(angle), depth(depth), height(height), num(num) {}
    std::vector<double> data;
    std::vector<std::vector<double>> center;
    std::vector<std::vector<double>> list_rote;
    std::vector<double> center_new;
    std::vector<double> rote;
    std::string LSystem;
    std::vector<std::string> tree;
    void draw() {
        for (int i = 0; i < depth; i++) {
            expand(str, num);
        }
        LSystem = tree.at(depth-1);
        center = {};
        list_rote = {};
        rote = { (float)glm::pi<float>() / 2, (float)glm::pi<float>() / 2, 0 };
        center_new = { 0, 0, 0 };
        //LSystem = "DDDD";
        std::cout << LSystem;
        std::string ch = "";
        for (int i = 0; i < LSystem.length(); i++) {
            ch = LSystem.at(i);

            if (ch.compare("D") == 0 || ch.compare("X") == 0) {
                std::vector<double> a = drawbranch(radius, height, rote[0], rote[1], rote[2],
                    center_new[0], center_new[1], center_new[2]);
                data.insert(data.end(), a.begin(), a.end());
            }
            else if (ch.compare("[") == 0) {
                push(radius);
                center.push_back(center_new);
                list_rote.push_back(rote);
            }
            else if (ch.compare("]") == 0) {
                pop(radius);
                if (center.size() > 1) {
                    center.pop_back();
                }
                center_new = center.back();
                rote = list_rote.back();
            }
            else if (ch.compare("V") == 0) {
                continue;
                //leaf();
            }
            else if (ch.compare("R") == 0) {
                rotR(angle);
            }
            else if (ch.compare("L") == 0) {
                rotL(angle);
            }
        }
    }

    void expand(std::string &str, float num) {
        std::string ch = "";

        for (int i = 0; i < str.length(); i++) {
            ch = str.at(i);

            if (ch.compare("D") == 0) {
                str.replace(i, 1, "DD");
                i = i + 1;
            }
            else if (ch.compare("X") == 0) {

                if (num < 0.4) {
                    //LSystem.replace(i, 1, "D[LX]D[RX]LX");
                    str.replace(i, 1, "D[LXV]D[RXV]LX");

                }
                else {
                    //LSystem.replace(i, 1, "D[RX]D[LX]RX");
                    str.replace(i, 1, "D[RXV]D[LXV]RX");
                }
                i = i + 13;	//11
            }

        }
        tree.push_back(str);
    }

    std::vector<double> drawbranch(float radius, float height, double& angle_x, double& angle_y, double& angle_z,
        double& center_x, double& center_y, double& center_z, unsigned int sectorCount = 360)
    {
        std::vector<double> data;
        double x1, y1, z1;
        double x2, y2, z2;
        /*for (int i = 0; i <= sectorCount; i += 1) {
            double u1 = i / (double)sectorCount;
            double u2 = (i + 1) / (double)sectorCount;
            x1 = (radius * cos(2 * (float)glm::pi<float>() * u1) + center_x) * cos(angle_x);
            y1 = (radius * sin(2 * (float)glm::pi<float>() * u1) + center_y) * cos(angle_y);
            z1 = center_z * cos(angle_z);

            x2 = (radius * cos(2 * (float)glm::pi<float>() * u2) + center_x) * cos(angle_x);
            y2 = (radius * sin(2 * (float)glm::pi<float>() * u2) + center_y) * cos(angle_y);
            z2 = (center_z + height) * cos(angle_z);

            data.push_back(x1);  data.push_back(y1); data.push_back(z1);
            data.push_back(u1);  data.push_back(0);

            data.push_back(x1);  data.push_back(y1); data.push_back(z2);
            data.push_back(u1);  data.push_back(0);

            data.push_back(x2);  data.push_back(y2); data.push_back(z1);
            data.push_back(u2);  data.push_back(1);

            data.push_back(x2);  data.push_back(y2); data.push_back(z1);
            data.push_back(u2);  data.push_back(1);

            data.push_back(x1);  data.push_back(y1); data.push_back(z2);
            data.push_back(u1);  data.push_back(0);

            data.push_back(x2);  data.push_back(y2); data.push_back(z2);
            data.push_back(u2);  data.push_back(0);


        }*/

        x2 = height * cos(angle_x) + center_x;
        y2 = height * cos(angle_y) + center_y;
        z2 = height * cos(angle_z) + center_z;
        data.push_back(center_x);  data.push_back(center_y); data.push_back(center_z);
        data.push_back(0);  data.push_back(0);
        data.push_back(x2);  data.push_back(y2); data.push_back(z2);
        data.push_back(1);  data.push_back(0);
        center_new = { x2, y2, z2 };
        return data;
    }

    void push(float radius) {
        if (radius > 0)
            radius -= 0.01;
    }

    void pop(float radius) {
        if (radius > 0)
            radius += 0.01;
    }

    void rotL(double angle) {
        rote[0] += angle;
        rote[1] += (4 *angle);
        rote[2] += angle;
    }

    void rotR(double angle) {
        rote[0] -= angle;
        rote[1] += (4 * angle);
        rote[2] -= angle;
    }


private:
    std::string str;
    unsigned int depth;
    float radius;
    double angle;
    float height;
    float num;

};

MeshPtr makeTree(std::string str, float angle, int depth, float radius, float height, float num);